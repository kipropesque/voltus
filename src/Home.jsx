import React from 'react';
import './App.css';
import FaceDetect from './FaceDetect/FaceDetect';

function Home() {
  return (
    <div className="App">
      <header className="App-header">
        <p>
          Voltus Search
        </p>
      <FaceDetect></FaceDetect>
      </header>
    </div>
  );
}

export default Home;
