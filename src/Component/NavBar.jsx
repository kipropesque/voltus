import React from "react";
import { Nav } from "reactstrap";
import { NavLink } from "react-router-dom";

const NavBar = () => {
  return (
    <Nav className="navbar navbar-expand-lg navbar-light bg-light">
      <NavLink className="navbar-brand" to="/">
        Home
      </NavLink>
      <NavLink className="navbar-brand" to="/people">
        People
      </NavLink>
      <NavLink className="navbar-brand" to="/images">
        Images
      </NavLink>
      <NavLink className="navbar-brand" to="/countries">
        Countries
      </NavLink>
    </Nav>
  );
};

export default NavBar;
