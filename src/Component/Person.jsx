import { personRetriever } from '../Utilities/backend';
import React, { Component } from 'react'

export default class Person extends Component {

    state = {
		isLoaded: false,
		person: []
	};

    componentDidMount = () => {
        try {
            personRetriever(this.props.match.params.id)
                .then(response => {
                    this.setState({ person: response.data })
                }
            );
        } catch (e) {
            console.log("something went wrong");
        }
    }

    render() {
        const { name, description, image_url } = this.state.person;
        return (
            <div>
                Name: {name}
                <br/>
                Description: {description}
                <br/>
                Face Token: { this.state.person.face ?  this.state.person.face.face_token : "" }
                <br/>
                User ID: { this.state.person._id  }
                <br/>
                <hr/>
                <br/>
                <div className="personimg-container">
                    <img className="personimg" src={image_url} alt={name}/>
                </div>
            </div>
        )
    }
}