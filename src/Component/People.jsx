import React, { Component } from "react";
import { Table, FormGroup, Label, Input, Form } from "reactstrap";
import { peopleRetriever } from "../Utilities/backend";
import { Link } from "react-router-dom";

class People extends Component {
	state = {
		isLoaded: false,
		peopleList: []
	};

	render() {
		const { peopleList, isLoaded } = this.state;

		if (isLoaded) {
			return (
				<React.Fragment>
					<br/>
					<Form inline className="search">
						<FormGroup className="mb-2 mr-sm-2 mb-sm-0">
							<Label for="search" className="mr-sm-2">
								Search
							</Label>
							<Input type="search" name="search" id="search" />
						</FormGroup>
					</Form>
					<Table>
						<thead>
							<tr>
								<th>Names</th>
								<th>Description</th>
								<th>#</th>
							</tr>
						</thead>
						<tbody>
							{ peopleList.map((person, key) => (
								<tr key={key}>
									<td>{person.name} </td>
									<td>{person.description} </td>
									<td> 
										<Link className="btn btn-primary" to={`person/${person._id}`}>View</Link>
									</td>
								</tr>
							)) }
						</tbody>
					</Table>
				</React.Fragment>
			);
		} else {
			return <p>Loading...</p>;
		}
	}

	componentDidMount = () => {
		try {
			peopleRetriever()
				.then(response => {
					this.setState({ isLoaded: true, peopleList: response.data })
				}
			);
		} catch (e) {
			console.log("something went wrong");
		}
	}
}

export default People;
