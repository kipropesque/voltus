import React, { Component } from 'react';
import { faceRectangleHelper, validateUploadedFiles } from '../Utilities/helper.js';
import { cloudinaryUploadHelper } from '../Utilities/cloudinary.js';
import { facesearchHelper } from '../Utilities/facePlus.js';
import { saveFaceHelper, personRetriever, peopleMultiRetriever } from '../Utilities/backend.js';

class FaceDetect extends Component {
    state = {
        loading: true,
        uploading: false,
        validating: false,
        image: null,
        facestyles: [],
        results: [],
        person: [],
        errors: null
    }
    saveRef = React.createRef();

    fileHandler = e => {
        const files = e.target.files;
        const invalid = validateUploadedFiles(e.target.files);
        if (invalid) {
            this.setState({errors: invalid, image: null});
            return null;
        }

        this.setState({validating: true, errors: null});
        var reader = new FileReader();
        reader.onload = (e) => {
            this.setState({image: reader.result})
        }
        reader.readAsDataURL(files[0]);
    }

    getPerson = (users) => {
        const user_ids = [];
        
        users.map(user => {
            user_ids.push(user.user_id);
            return user_ids;
        });

        try {
            peopleMultiRetriever(user_ids)
                .then(response => {
                    this.setState({ person: response.data })
                }
            );
        } catch (e) {
            console.log("something went wrong");
        }
    }

    data = {
        "image_id": "tXGz5dKH3N2E5FDHIvD2Qw==",
        "faces": [
            {
                "face_rectangle": {
                    "width": 168,
                    "top": 107,
                    "left": 533,
                    "height": 168
                },
                "face_token": "72cfa64c80ff3511bbeabac4ec28736a"
            }
        ],
        "time_used": 389,
        "thresholds": {
            "1e-3": 62.327,
            "1e-5": 73.975,
            "1e-4": 69.101
        },
        "request_id": "1591270326,123aa890-c2a6-4117-9f02-15f46c104b3c",
        "results": [
            {
                "confidence": 85.356,
                "user_id": "eb5c240f-75d5-4c48-9230-cdb00cbbbbeb",
                "face_token": "c89e9af46cb83e417d8345faab205f64"
            },
            {
                "confidence": 85.356,
                "user_id": "c6816cda-ab80-4779-9608-d51a7d91cb48",
                "face_token": "c89e9af46cb83e417d8345faab205f64"
            },
            {
                "confidence": 85.356,
                "user_id": "c6816cda-ab80-4779-9608-d51a7d91cb48",
                "face_token": "c89e9af46cb83e417d8345faab205f64"
            },
            {
                "confidence": 85.356,
                "user_id": "c6816cda-ab80-4779-9608-d51a7d91cb48",
                "face_token": "c89e9af46cb83e417d8345faab205f64"
            }
        ]
    }

    faceHandler = async () => {
        //send to FPP for detection
        if( this.state.validating === true ) {
            // const faceSearchData = await facesearchHelper(this.state.image);
            // this.setState({results: faceSearchData.results});
            //retrieve details of this person 
            this.getPerson(this.data.results);
            //map face rectangles on image
            // const faceStyles = faceRectangleHelper(faceSearchData);
            // this.setState({facestyles: faceStyles});
            //upload image to cloudinary
            // const cloudinaryUrl = await cloudinaryUploadHelper(this.state.image);
            //save to backend
            // saveFaceHelper(faceSearchData, cloudinaryUrl);
        }
    }
    
    render() {
        const { image, facestyles, errors, results, person } = this.state;
        return (
            <div className="container">
            <div className="row">
                <div className="col-md-6">
                        <div className="form-group files color">
                            <label>Upload Your File </label>
                            <input type="file" className="form-control" name="file" onChange={this.fileHandler}/>
                        </div>
                </div>
                <div className="col-md-6 subface"><button type="button" onClick={this.faceHandler} className="btn btn-primary">Detect</button></div>
                <ul>
                    { errors ? (
                        errors.map((error, i) => 
                        <li key={i}>
                            {error}
                        </li>
                    )) : "" }
                </ul>
            </div>
            { results.map(result => 
                <div className="left">
                    <p>Results Confidence: { result.confidence } </p>
                    <p>Face Token: { person.name }</p>
                </div>
            ) }
            
            <div className="row"> 
                <div id="image-container" className="image-container col-md-12">
                    { image ? (
                            <img src={image} className="faceimg" id="faceimg" onClick={this.imageHandler} alt="faceinfo"></img>
                    ) : ("") }
                    { facestyles.map((facestyle, i) => 
                        <div key={i} style={facestyle}></div> 
                    )}
                </div>
            </div>
        </div>
        )
    }
}

export default FaceDetect;
