import React, { Component } from 'react';

class ImageContainer extends Component {

    componentDidMount() {
        const { current } = this.props;
        console.log(`${current.offsetWidth}, ${current.offsetHeight}`);
    }

    render() { 
        return ( <img ref={this.props} src={this.props.image} className="faceimg" alt="faceinfo"></img> );
    }
}
 
export default ImageContainer;