import React from 'react';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Home from "./Home";
import People from "./Component/People";
import Person from "./Component/Person";
import NavBar from './Component/NavBar'

import { Container } from "reactstrap";
import { Route, Switch, Redirect } from "react-router-dom";

function App() {
  return (
    <Container className="App">
        <NavBar />
        <Switch>
          <Route path="/home" component={Home} />
          <Route path="/people" component={People} />
          <Route path='/person/:id' component={Person} />
          <Redirect exact from="/" to="/home" />
          <Redirect to="/not-found" />
        </Switch>
      </Container>
  );
}

export default App;
