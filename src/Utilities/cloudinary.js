//Cloudinary shit
import axios from 'axios';

export const cloudinaryUploadHelper = async (image) => {
    const cloudinaryUrl = process.env.REACT_APP_CLOUDINARY_URL;
    
    const cloudinaryConfig = {
        upload_preset: "voltus",
        file: image
    }
    
    try {
        const res = await axios.post(cloudinaryUrl, cloudinaryConfig )
        return res.data.url;
    } catch (err) {
        console.log(err);
    }
}