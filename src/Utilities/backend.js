//Collection of backend helpers.
import axios from 'axios';

export const saveFaceHelper = (faceDetails, cloudinaryUrl) => {
    //save image details
    const backendUrl = process.env.REACT_APP_BACKEND_URL.concat("/api/image");
    
    const imageData = {
        cloudinary_url: cloudinaryUrl,
        faceplus_image_id: faceDetails.image_id,
        face_num: faceDetails.face_num,
        faces: faceDetails.faces
    };

    axios.post( backendUrl, imageData )
    .then(res => {
        console.log(res);
    }).catch(err => {
        console.log(err);
    });
}

export const peopleRetriever = async () => {
	const url = process.env.REACT_APP_BACKEND_URL.concat('/api/person');

    try {
        const response = await axios.get(url);
        return response;
    } catch (err) {
        console.log("Uh oh, we have in /api/person:", err);
    }
};

export const personRetriever = async (personId) => {
	const url = process.env.REACT_APP_BACKEND_URL.concat('/api/person/').concat(personId);

    try {
        const response = await axios.get(url);
        return response;
    } catch (err) {
        console.log("Uh oh, we have in /api/person/:id:", err);
    }
};

export const peopleMultiRetriever = async (user_ids) => {
	const url = process.env.REACT_APP_BACKEND_URL.concat('/api/person/multi');

    try {
        const response = await axios.post(url, {'user_ids': user_ids});
        return response;
    } catch (err) {
        console.log("Uh oh, we have in /api/person:", err);
    }
};
