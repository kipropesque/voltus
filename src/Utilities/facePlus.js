//Faceplus shit
import axios from 'axios';

export const facesearchHelper = async (image) => {
    const formData = new FormData();
    formData.append('api_key', process.env.REACT_APP_FP_API_KEY);
    formData.append('api_secret', process.env.REACT_APP_FP_API_SECRET);
    formData.append('image_base64', image);
    formData.append('faceset_token', process.env.REACT_APP_FP_FACESET_TOKEN);

    const url = process.env.REACT_APP_FP_FACE_API.concat('facepp/v3/search');
    
    //freeze submit button
    const fppResponse = await axios({
        method: 'post',
        url: url,
        data: formData,
        processData: false,
        contentType: false,
    });
    return fppResponse.data;
}