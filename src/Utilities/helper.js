
export const validateUploadedFiles = (uploadedFiles) => { 
    const errs = [] 
    const files = Array.from(uploadedFiles);

    if (files.length !== 1 ) {
		const msg = 'Only 1 images can be uploaded at a time';
		errs.push(msg)
		return errs;
	}

	const types = ['image/png', 'image/jpeg', 'image/gif']
	const file = files[0];

    if (types.every(type => file.type !== type)) {
        errs.push(`'${file.type}' is not a supported format`)
    }

    if (file.size > 5000000) {
        errs.push(`'${file.name}' is too large, please pick a smaller file`)
    }

    if(errs.length > 0) {
        return errs;
    }
    return false;
};

export const faceRectangleHelper = (res) => {
	const imageView = document.getElementById("faceimg");
	const imageContainer = document.getElementById("image-container");
	const faces = res.faces;
	let facestyles = [];

	for (const index in faces) {
		const face = faces[index];
		const face_rectangle = face.face_rectangle;

		//error handling
		var roll = face.attributes ? face.attributes.headpose.roll_angle : 0;

		//脸部坐标
		var faceX = face_rectangle.left;
		var faceY = face_rectangle.top;
		var faceW = face_rectangle.width;
		var faceH = face_rectangle.height;

		//faceContainer尺寸
		var width = 320;
		var height = 320;

		//img尺寸
		var imageW = imageView.width;
		var imageH = imageView.height;

		//图片实际尺寸
		var naturalWidth = imageView.naturalWidth;
		var naturalHeight = imageView.naturalHeight;

		console.log("container尺寸" + width + "----" + height);
		console.log("img尺寸" + imageW + "----" + imageW);
		console.log("图片实际尺寸" + naturalWidth + "----" + imageH);

		const scale = imageW / naturalWidth;
		const scalew = imageW / naturalWidth;
		const scaleh = imageH / naturalHeight;

		console.log("scale > " + scale);

		const offsetX = (imageContainer.offsetWidth - imageW) * 0.5;
		const offsetY = (imageContainer.offsetHeight - imageH) * 0.5;

		console.log("offsetX：" + offsetX + "offsetY: " + offsetY);

		//添加人脸框
		facestyles.push({
			position: "absolute",
			top: faceY * scaleh + offsetY,
			left: faceX * scalew + offsetX,
			height: faceH * scaleh,
			width: faceW * scalew,
			border: "2px solid blue",
			transform: "rotate(" + roll + "deg)",
		});
	}
	return facestyles;
};
